#!/usr/bin/python

import urllib2
import re
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--iterations", help="Number of times to query the balancer", type=int)
args = parser.parse_args()

servername_regex = re.compile('answered by server (.*?)<')

server_responses = dict()

for i in xrange(0, args.iterations):
    response = urllib2.urlopen("http://balancer").read()

    matches = servername_regex.search(response)

    if matches:
	servername = matches.group(1)
	if servername in server_responses:
	    server_responses[servername] += 1
	else:
	    server_responses[servername] = 1

for server, count in server_responses.iteritems():
    print "Server %s has responded %d times" % (server, count)
